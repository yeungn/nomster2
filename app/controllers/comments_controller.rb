class CommentsController < ApplicationController
	before_action :authenticate_user!, :only => [:create]
	def create
		@place = Place.find(params[:place_id])
		@place.comments.create(comment_params.merge(:user => current_user))
		redirect_to place_path(@place)
	end

	def destroy
		@comment = Comment.find(params[:id])
		@comment = Place.find(params[:place_id])
		if @comment.destroy
			redirect_to place_path(@place), notice: 'comment has been deleted'
		else
			redirect_to place_path(@place), notice: 'error deleting'
		end
	end

	private

	def comment_params
		params.require(:comment).permit(:message, :rating)
	end
end
