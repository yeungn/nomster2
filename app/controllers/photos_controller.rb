class PhotosController < ApplicationController
	before_action :authenticate_user!, :only => [:create]
	def create
		@place = Place.find(params[:place_id])
		@place.photos.create(photo_params)
		redirect_to place_path(@place)
	end

	def destroy
		puts "destroy me"
		@photo = Photo.find(params[:id])
		@place = Place.find(params[:place_id])
		if @photo.destroy
			redirect_to place_path(@place), notice: 'photo has been deleted'
		else
			redirect_to place_path(@place), notice: 'delete error'
		end
	end

	private

	def photo_params
		params.require(:photo).permit(:caption, :picture)
	end
end
