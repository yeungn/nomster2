FactoryGirl.define do

	factory :user do
		email 'example@test.com'
		password 'password'
	end

	factory :place do 
		name 'place_name'
		address '123 main'
		description 'hello'
	end

	factory :comment do
		rating '3_stars'
		user
		place
	end
	
end

