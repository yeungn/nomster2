require 'test_helper'

class CommentsControllerTest < ActionController::TestCase
	test 'adding comment' do
		user = create :user
		sign_in user
		place = create :place, user: user
    post :create, :place_id => place.id, :comment => {:message => 'working?', :rating => '5_stars'}
    assert_redirected_to place_path
	end
end

