require 'test_helper'

class UsersControllerTest < ActionController::TestCase
	test 'GET user show: redirect for unauthenticated users' do
		user = create :user
		get :show, {id: user.id}
		assert_response :redirect
		assert_redirected_to new_user_session_path
	end

	test 'GET user show: allow access for signed in users' do
		user = create :user
		sign_in user
		get :show, {id: user.id}
		assert_response :success
	end
end
