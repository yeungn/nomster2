require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  test "rating_test" do
  	comment = build(:comment, rating: '1_star')
  	assert_equal 'one star', comment.humanized_rating
  end
end